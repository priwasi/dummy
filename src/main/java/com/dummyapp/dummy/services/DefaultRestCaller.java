package com.dummyapp.dummy.services;

import com.dummyapp.dummy.models.restcaller.RestResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.*;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.util.Strings;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class DefaultRestCaller extends AbstractRestApiService implements RestCaller {

    private static final Logger LOGGER = LogManager.getLogger(DefaultRestCaller.class);
    private static final ObjectMapper objectMapper = new ObjectMapper();

    static ConnectionKeepAliveStrategy connectionKeepAliveStrategy = new ConnectionKeepAliveStrategy() {
        @Override
        public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
            HeaderElementIterator it = new BasicHeaderElementIterator
                    (response.headerIterator(HTTP.CONN_KEEP_ALIVE));
            while (it.hasNext()) {
                HeaderElement he = it.nextElement();
                String param = he.getName();
                String value = he.getValue();
                if (value != null && param.equalsIgnoreCase
                        ("timeout")) {
                    return Long.parseLong(value) * 1000;
                }
            }
            return (long) 5 * 1000;
        }
    };

    private static PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();

    private static CloseableHttpClient client = HttpClients.custom().setDefaultRequestConfig(RequestConfig.custom().setStaleConnectionCheckEnabled(true).setConnectTimeout(1000).setConnectionRequestTimeout(1000).build()).setKeepAliveStrategy(connectionKeepAliveStrategy).setConnectionManager(connManager).build();

    public static void init() {
        connManager.setDefaultMaxPerRoute(50);
        connManager.setMaxTotal(100);
        connManager.setDefaultSocketConfig(SocketConfig.custom().setSoTimeout(800).build());

        LOGGER.debug("Default Rest Caller Config done ");
    }


    public static final String NAME = "defaultRestCaller";

    private String requestId = null;

    private String metricId;

    public DefaultRestCaller(String metricId) {
        this.metricId = metricId;
    }

    public DefaultRestCaller(String metricId, String requestId) {
        this(metricId);
        this.requestId = requestId;
    }

    @Override
    public RestResponse restCall(boolean isLog, String body) throws Exception {

        LOGGER.debug(String.format("Calling url %s with body %s", this.getUrl(), this.getPostBody()));

        this.validateParameters();
        HttpResponse response = null;
        Header[] responseHeaders;
        Map<String, String> responseHeadersMap;
        String responseBody;

        double start =  System.currentTimeMillis();

        try {
            switch (this.getMethod()) {
                case GET:
                    HttpGet httpGet = new HttpGet(this.getUrl() + "?" + this.getQueryParamsString());
                    setRequestHeaders(httpGet);
                    response = client.execute(httpGet);
                    break;
                case POST:
                    HttpPost httpPost = new HttpPost(this.getUrl() + "?" + this.getQueryParamsString());
                    setRequestHeaders(httpPost);
                    if (body == null) {
                        body = objectMapper.writeValueAsString(this.getPostBody());
                    }
                    httpPost.setEntity(new StringEntity(body));
                    response = client.execute(httpPost);
                    break;
                case PUT:
                    HttpPut httpPut = new HttpPut(this.getUrl() + "?" + this.getQueryParamsString());
                    setRequestHeaders(httpPut);
                    httpPut.setEntity(new StringEntity(objectMapper.writeValueAsString(this.getPostBody())));
                    response = client.execute(httpPut);
                    break;
                case PATCH:
                    HttpPatch httpPatch = new HttpPatch(this.getUrl() + "?" + this.getQueryParamsString());
                    setRequestHeaders(httpPatch);
                    httpPatch.setEntity(new StringEntity(objectMapper.writeValueAsString(this.getPostBody())));
                    response = client.execute(httpPatch);
                    break;
                case DELETE:
                    HttpDelete httpDelete = new HttpDelete(this.getUrl() + "?" + this.getQueryParamsString());
                    setRequestHeaders(httpDelete);
                    response = client.execute(httpDelete);
            }

            double now = System.currentTimeMillis();

            if (response == null) {
                return null;
            }
            if(LOGGER.isDebugEnabled()) {
                LOGGER.debug(String.format("url: %s : request Id: %s, response: %s", this.getUrl() + "?" + this.getQueryParamsString(), requestId, response.getStatusLine().getStatusCode()));
            }
            responseHeaders = response.getAllHeaders();
            responseHeadersMap = new HashMap<>();
            responseBody = EntityUtils.toString(response.getEntity());
            if (isLog && responseBody != null && LOGGER.isDebugEnabled()) {
                if (responseBody.length() >= 300) {
                    LOGGER.debug(String.format("response body: %s", responseBody.substring(0, 300)));
                } else {
                    LOGGER.debug(String.format("response body: %s", responseBody));
                }
            }

            for (Header header : responseHeaders) {
                responseHeadersMap.put(header.getName(), header.getValue());
            }

            try {
                return (new RestResponse(response.getStatusLine().getStatusCode(), responseBody == null || responseBody.isEmpty() ? null
                        : objectMapper.readValue(responseBody, Object.class), responseHeadersMap));
            } catch (Exception e) {
                LOGGER.error("Error in rest call ",e);
                throw new Exception(String.valueOf(responseBody));
            }
        }catch (SocketTimeoutException e){
            LOGGER.warn(String.format("Read time out on restcall %s",this.getUrl()));
            throw e;
        } catch (ConnectTimeoutException e) {
            LOGGER.warn(String.format("Connect time out on restcall %s",this.getUrl()));
            throw e;
        }finally {
            if(response != null) {
                LOGGER.debug("Going to close rest response");
                ((CloseableHttpResponse) response).close();
            }

         }
    }

    public String getQueryParamsString(Map<String, String> queryParams) {
        StringBuilder sb = new StringBuilder();
        if (queryParams == null) {
            return Strings.EMPTY;
        }
        for (Map.Entry<?,?> entry : queryParams.entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(String.format("%s=%s",
                    urlEncodeUTF8(entry.getKey().toString()),
                    entry.getValue() == null ? null : urlEncodeUTF8(entry.getValue().toString())
            ));
        }
        return sb.toString();
    }

    private void setRequestHeaders(HttpRequestBase httpRequestBase) {
        if (this.getHeaders() != null && !this.getHeaders().isEmpty()) {
            this.getHeaders().forEach(httpRequestBase::setHeader);
        }
    }

    private String getQueryParamsString() {
        StringBuilder sb = new StringBuilder();
        if (this.getRequestParams() == null) {
            return Strings.EMPTY;
        }
        for (Map.Entry<?,?> entry : this.getRequestParams().entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(String.format("%s=%s",
                    urlEncodeUTF8(entry.getKey().toString()),
                    entry.getValue() == null ? null : urlEncodeUTF8(entry.getValue().toString())
            ));
        }
        return sb.toString();
    }

    private static String urlEncodeUTF8(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public String getMetricId() {
        return metricId;
    }

    public void setMetricId(String metricId) {
        this.metricId = metricId;
    }

    public static void closeConn(){

        LOGGER.debug(String.format("Closing DefaultRestCaller connection"));

        try {
            LOGGER.debug(String.format("Closing Client"));
            client.close();
            LOGGER.debug(String.format("Client Closed"));

        } catch (IOException e) {
            LOGGER.error("Error while closing pooling connection client",e);
        }
        connManager.close();

        LOGGER.debug(String.format("DefaultRestCaller connection closed"));

    }

}
