package com.dummyapp.dummy.services;


import com.dummyapp.dummy.models.RequestBodyDTO;
import org.springframework.stereotype.Service;

@Service
public interface RequestHandler {

    void handleReadRequest(RequestBodyDTO requestBodyDTO, String encryptedSymKey, String userPublicKey);
    void handleDeleteRequest(RequestBodyDTO requestBodyDTO);

}
