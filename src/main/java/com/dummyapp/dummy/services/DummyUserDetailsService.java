package com.dummyapp.dummy.services;


import com.dummyapp.dummy.models.DummyUserDetails;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class DummyUserDetailsService implements UserDetailsService {


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        DummyUserDetails dummyUserDetails = new DummyUserDetails();
        dummyUserDetails.setUserId("Andrew");
        dummyUserDetails.setName("Andrew Smith");
        dummyUserDetails.setEmail("andrew@dummy.com");
        return dummyUserDetails;
    }


}
