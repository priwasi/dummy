package com.dummyapp.dummy.services;


import org.springframework.stereotype.Service;

@Service
public interface RequestHandlerFactory {

    RequestHandler getRequestHandler(int datasourceId);

}
