package com.dummyapp.dummy.services.impl;

import com.dummyapp.dummy.config.CredBoxConfig;
import com.dummyapp.dummy.models.RequestBodyDTO;
import com.dummyapp.dummy.models.restcaller.HttpMethod;
import com.dummyapp.dummy.services.DefaultRestCaller;
import com.dummyapp.dummy.services.RequestHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;


@Service
@Qualifier("firstDatasourceRequestHandler")
public class FirstDatasourceRequestHandler implements RequestHandler {

    private static final Logger LOGGER = LogManager.getLogger(FirstDatasourceRequestHandler.class);

    @Autowired private CredBoxConfig credBoxConfig;

    @Override
    public void handleReadRequest(RequestBodyDTO requestBodyDTO, String encryptedSymKey, String userPublicKey) {
        delayed(encryptedSymKey, userPublicKey);
    }

    private void delayed(String encryptedSymKey, String userPublicKey) {
        Thread t = new Thread(() -> {
            DefaultRestCaller defaultRestCaller = new DefaultRestCaller("read");
            defaultRestCaller.setUrl(credBoxConfig.getUrl());
            defaultRestCaller.setMethod(HttpMethod.POST);
            Map<String, String> headers = new HashMap<>();
            headers.put("clientId", credBoxConfig.getClientId());
            headers.put("clientSecret", credBoxConfig.getClientSecret());
            headers.put("encryptedSymKey", encryptedSymKey);
            headers.put("userPublicKey", userPublicKey);
            defaultRestCaller.setHeaders(headers);
            String body = "{ \"data\": { \"records\": [{ \"recordId\": 1129, \"transactionType\": \"bank\", \"transactionAmount\": \"$100\"" +
                    ", \"transactionTimestamp\": \"2019-12-21T10:01:16\" }, { \"recordId\": 2146, \"transactionType\": \"direct transfer\"" +
                    ", \"transactionAmount\": \"$550\", \"transactionTimestamp\": \"2019-12-20T17:14:10\" }, { \"recordId\": 3376" +
                    ", \"transactionType\": \"wallet\", \"transactionAmount\": \"$130\", \"transactionTimestamp\": \"2019-12-20T11:24:33\" } ] } }";
            try {
                Thread.sleep(20000);
                defaultRestCaller.restCall(false, body);
            } catch (Exception e) {
                LOGGER.error("Could not handle first datasource read request due to exception: ", e);
            }
        });
        t.start();
    }

    @Override
    public void handleDeleteRequest(RequestBodyDTO requestBodyDTO) {
    }
}
