package com.dummyapp.dummy.services.impl;


import com.dummyapp.dummy.config.CredBoxConfig;
import com.dummyapp.dummy.models.RequestBodyDTO;
import com.dummyapp.dummy.models.restcaller.HttpMethod;
import com.dummyapp.dummy.services.DefaultRestCaller;
import com.dummyapp.dummy.services.RequestHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Qualifier("secondDatasourceRequestHandler")
public class SecondDatasourceRequestHandler implements RequestHandler {

    private static final Logger LOGGER = LogManager.getLogger(SecondDatasourceRequestHandler.class);

    @Autowired private CredBoxConfig credBoxConfig;

    @Override
    public void handleReadRequest(RequestBodyDTO requestBodyDTO, String encryptedSymKey, String userPublicKey) {
        delayed(encryptedSymKey, userPublicKey);
    }


    private void delayed(String encryptedSymKey, String userPublicKey) {
        Thread t = new Thread(() -> {
            DefaultRestCaller defaultRestCaller = new DefaultRestCaller("read");
            defaultRestCaller.setUrl(credBoxConfig.getUrl());
            defaultRestCaller.setMethod(HttpMethod.POST);
            Map<String, String> headers = new HashMap<>();
            headers.put("clientId", credBoxConfig.getClientId());
            headers.put("clientSecret", credBoxConfig.getClientSecret());
            headers.put("encryptedSymKey", encryptedSymKey);
            headers.put("userPublicKey", userPublicKey);
            defaultRestCaller.setHeaders(headers);

            String body = "{ \"data\": { \"records\": [{ \"recordId\": \"1142\", \"name\": \"Alice\", \"email\": \"alice@wonderland.com\"" +
                    ", \"contactNumber\": \"9999999999\", \"gender\": \"Female\", \"date of birth\": \"1999-01-14\" }] } }";
            try {
                Thread.sleep(40000);
                defaultRestCaller.restCall(false, body);
            } catch (Exception e) {
                LOGGER.error("Could not handle second datasource read request due to exception: ", e);
            }
        });
        t.start();
    }

    @Override
    public void handleDeleteRequest(RequestBodyDTO requestBodyDTO) {
        return;
    }
}
