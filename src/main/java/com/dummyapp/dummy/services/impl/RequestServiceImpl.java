package com.dummyapp.dummy.services.impl;

import com.dummyapp.dummy.models.RequestBodyDTO;
import com.dummyapp.dummy.services.RequestHandler;
import com.dummyapp.dummy.services.RequestHandlerFactory;
import com.dummyapp.dummy.services.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


@Service
public class RequestServiceImpl implements RequestService {

    @Autowired private RequestHandlerFactory requestHandlerFactory;

    @Autowired
    @Qualifier("firstDatasourceRequestHandler")
    private RequestHandler firstRequestHandler;

    @Autowired
    @Qualifier("secondDatasourceRequestHandler")
    private RequestHandler secondRequestHandler;


    @Override
    public void handleRequest(int apiId, RequestBodyDTO requestBodyDTO, String encryptedSymKey, String userPublicKey) {
        switch (apiId) {
            case 1:
                firstRequestHandler.handleReadRequest(requestBodyDTO, encryptedSymKey, userPublicKey);
                break;
            case 2:
                secondRequestHandler.handleReadRequest(requestBodyDTO, encryptedSymKey, userPublicKey);
                break;
        }
    }
}
