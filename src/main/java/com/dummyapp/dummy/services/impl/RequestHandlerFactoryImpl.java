package com.dummyapp.dummy.services.impl;

import com.dummyapp.dummy.services.RequestHandler;
import com.dummyapp.dummy.services.RequestHandlerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;


@Service
public class RequestHandlerFactoryImpl implements RequestHandlerFactory {

    private Map<Integer, RequestHandler> requestHandlerMap;

    @Autowired
    @Qualifier("firstDatasourceRequestHandler")
    private RequestHandler firstDatasourceRequestHandler;

    @Autowired
    @Qualifier("secondDatasourceRequestHandler")
    private RequestHandler secondDatasourceRequestHandler;

    @PostConstruct
    public void init() {
        requestHandlerMap = new HashMap<>();
        requestHandlerMap.put(1, firstDatasourceRequestHandler);
        requestHandlerMap.put(2, secondDatasourceRequestHandler);
    }

    @Override
    public RequestHandler getRequestHandler(int datasourceId) {
        return requestHandlerMap.get(datasourceId);
    }
}
