package com.dummyapp.dummy.services;

import com.dummyapp.dummy.models.restcaller.RestResponse;
import org.apache.http.client.methods.HttpPost;

import java.io.IOException;

public interface RestCaller {
    RestResponse restCall(boolean isLog, String body) throws Exception;
}