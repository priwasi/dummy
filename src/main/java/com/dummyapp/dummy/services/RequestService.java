package com.dummyapp.dummy.services;

import com.dummyapp.dummy.models.RequestBodyDTO;
import org.springframework.stereotype.Service;


@Service
public interface RequestService {

    void handleRequest(int apiId, RequestBodyDTO requestBodyDTO, String encryptedSymKey, String userPublicKey);

}
