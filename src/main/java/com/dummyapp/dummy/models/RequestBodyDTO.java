package com.dummyapp.dummy.models;

public class RequestBodyDTO {

    private String userId;
//    private RequestType requestType;
    private int dsId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

//    public RequestType getRequestType() {
//        return requestType;
//    }
//
//    public void setRequestType(RequestType requestType) {
//        this.requestType = requestType;
//    }

    public int getDsId() {
        return dsId;
    }

    public void setDsId(int dsId) {
        this.dsId = dsId;
    }
}
