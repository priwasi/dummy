package com.dummyapp.dummy.models.restcaller;

public enum HttpMethod {
    GET,
    POST,
    PUT,
    PATCH,
    DELETE;

    HttpMethod() {
    }
}