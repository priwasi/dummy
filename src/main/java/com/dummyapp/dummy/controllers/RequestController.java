package com.dummyapp.dummy.controllers;


import com.dummyapp.dummy.models.RequestBodyDTO;
import com.dummyapp.dummy.services.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/internal/request")
public class RequestController {

    @Autowired private RequestService requestService;

    @RequestMapping("/1")
    @ResponseBody
    public ResponseEntity getRequest1(@RequestParam("dsId") String dsId, @RequestBody RequestBodyDTO requestBody, HttpServletRequest request) {

        String encryptedSymKey = request.getHeader("encryptedSymKey");
        String userPublicKey = request.getHeader("userPublicKey");

        requestService.handleRequest(1, requestBody, encryptedSymKey, userPublicKey);
        return ResponseEntity.ok("done");
    }

    @RequestMapping("/2")
    @ResponseBody
    public ResponseEntity getRequest2(@RequestParam("dsId") String dsId, @RequestBody RequestBodyDTO requestBody, HttpServletRequest request) {

        String encryptedSymKey = request.getHeader("encryptedSymKey");
        String userPublicKey = request.getHeader("userPublicKey");

        requestService.handleRequest(2, requestBody, encryptedSymKey, userPublicKey);
        return ResponseEntity.ok("done");
    }

}