package com.dummyapp.dummy.config.securityConfig;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;
import java.util.Collections;

public class InternalClientFilter extends GenericFilterBean {

    private static final Logger LOGGER = LogManager.getLogger(InternalClientFilter.class);

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        LOGGER.info("Inside internal filter");
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(null, new User("elayer", "", Collections.emptyList()), Collections.emptyList()));
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
